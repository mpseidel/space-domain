import {BaseCommand} from "../baseCommands";

export function createHandler(getCurrentUser, handleCommand, denormalizer) {
  const {denormalizerStatus, errors} = denormalizer;

  return function internalHandleCommand(cmd: BaseCommand, req: any, observeEvents?) {
    return new Promise<any>((resolve, reject) => {

      if (!req)
        return reject('missing req parameter');

      if (cmd.validate) {
        const validationErrors = cmd.validate();
        if (validationErrors.length > 0) {
          return reject('validation errors: ' + JSON.stringify(validationErrors));
        }
      }

      let cmdJson = cmd.toJson();
      if (getCurrentUser) {
        let currentUser = getCurrentUser(req);
        if (cmdJson.payload.createdBy && cmdJson.payload.createdBy !== currentUser) {
          return reject('createdBy property of command does not match current user');
        }
        cmdJson.payload.createdBy = currentUser;
      }
      else if (cmdJson.payload.createdBy) {
        return reject('could not validate current user');
      }

      function removeListeners() {
        errors.removeAllListeners();
        denormalizerStatus.removeAllListeners();
      }

      let errorHandler = function cmdError(err) {
        removeListeners();
        return reject(new Error(err));
      };

      errors.on('error', errorHandler);
      errors.on('commandError', errorHandler);

      denormalizerStatus.on('completedDenormalizing', (evt) => {
        if (observeEvents) {
          observeEvents(evt);
        }
      });

      handleCommand(cmdJson, (err, result) => {
        if (denormalizer.denormalizersActive > 0) {
          denormalizerStatus.on('completedDenormalizing', () => {
            if (denormalizer.denormalizersActive === 0) {
              removeListeners();

              if (err) {
                return reject(err);
              }

              resolve(result);
            }
          });
        } else {

          removeListeners();

          if (err) {
            return reject(err);
          }

          resolve(result);
        }
      });
    });
  }
}