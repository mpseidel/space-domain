"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function createDispatcher(denormalizer, synchronousDenormalizer) {
    const { denormalizerStatus } = synchronousDenormalizer;
    function handleEvent(evt) {
        denormalizer.handle(evt, () => {
            synchronousDenormalizer.denormalizersActive--;
            denormalizerStatus.emit('completedDenormalizing', evt);
        });
    }
    function handleSequentially(evt) {
        if (synchronousDenormalizer.denormalizersActive > 1) {
            denormalizerStatus.once('completedDenormalizing', () => handleSequentially(evt));
        }
        else {
            handleEvent(evt);
        }
    }
    return function (evt) {
        synchronousDenormalizer.denormalizersActive++;
        denormalizerStatus.emit('startedDenormalizing', evt);
        handleSequentially(evt);
    };
}
exports.createDispatcher = createDispatcher;
//# sourceMappingURL=eventDispatcher.js.map