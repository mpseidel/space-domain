export function defineDenormalizerSchema(denormalizer) {
  denormalizer.defineEvent({
    correlationId: 'correlationId',
    id: 'id',
    name: 'event',
    aggregateId: 'aggregate.id',
    context: 'context.name',
    aggregate: 'aggregate.name',
    payload: 'payload',
    revision: 'revision',
    version: 'version',
    meta: 'meta'
  });
}
