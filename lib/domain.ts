import {config} from "@ms3/space-config";
const debug = require('debug')('space-domain');

import objectAssign = require("object-assign");
import {createHandler} from "./commands/commandHandler";
import {createDispatcher} from "./commands/eventDispatcher";
import {defineCqrsDomainSchema} from "./domain/cqrsDomainSchema";
import {defineDenormalizerSchema} from "./denormalizer/denormalizerSchema";

const path = require('path');
const fs = require('fs');
const _ = require('lodash');
const events = require('events');
require('reflect-metadata');

export let eventDenormalizer = undefined;
let cqrsDomain = undefined;

const domainHelper = require('cqrs-domain');
const denormalizerHelper = require('cqrs-eventdenormalizer');

let getCurrentUser = (request:any) => 'anonymous';

const commands = {};
let viewBuilders = {};
let _options:any = {};

let errors = new events.EventEmitter();
let denormalizerStatus = new events.EventEmitter();

const synchronousDenormalizer = {
  errors,
  denormalizerStatus,
  denormalizersActive: 0
};

let partitionKeyResolver = () => undefined;
export let getPartitionKey = () => partitionKeyResolver();
export let getOptions = () => _options;

function handleCommand(cmd, cb){
  cqrsDomain.handle(cmd, cb);
}

export const domain:any = {
  getCommands: () => commands,

  init: function (options, cb) {
    _options = options;

    let domainPath = options.domainPath || process.cwd() + '/app/domain';

    getCurrentUser = options.getCurrentUser;
    partitionKeyResolver = options.getPartitionKey;

    cqrsDomain = require('cqrs-domain')({
      domainPath: domainPath,
      eventStore: options.eventStore,
      snapshotThreshold: 100000
    });

    defineCqrsDomainSchema(cqrsDomain);

    let denormalizer = require('cqrs-eventdenormalizer')({
      denormalizerPath: options.denormalizerPath || process.cwd() + '/app/domain',
      repository: options.readModelStore
    });

    defineDenormalizerSchema(denormalizer);

    eventDenormalizer = denormalizer;

    denormalizer.init(function (denormError) {

      if (denormError) {
        console.log('error in denormailzer.init', denormError);
        cb(denormError);
        return debug(denormError);
      }

      denormalizer.tree.getCollections().map(c => c.viewBuilders = []);
      viewBuilders = {};

      cqrsDomain.init(function (err) {

        if (err) {
          console.log('error in domain.init', err);
          cb(err);
          return debug(err);
        }

        let context = cqrsDomain.tree.getContexts()[0];

        let glob = require('glob');

        const aggregateFolders = glob.sync(domainPath + '/*');

        aggregateFolders.map(af => {

          let aggregateName = path.basename(af);

          let aggregatePath = path.join(af, aggregateName);
          let aggregateClass = null;

          if (!fs.existsSync(aggregatePath + '.js'))
            return;

          let aggregateModule = require(aggregatePath);
          aggregateClass = aggregateModule[Object.keys(aggregateModule)[0]];

          let aggregate = _.find(context.aggregates, ag => ag.name == aggregateName);

          aggregate.commands = [];
          aggregate.events = [];

          const searchPath = af + '/**/*.js';
          const domainFiles = glob.sync(searchPath);

          const commandBase = require('./baseCommands').Command;
          const eventBase = require('./events').Event;
          const viewBuilderBase = require('./viewbuilder').ViewBuilder;

          domainFiles.forEach((cf) => {

            let agc:any = require(cf);

            for (let commandClass in agc) {
              if (!agc.hasOwnProperty(commandClass))
                continue;

              let anyCommandClass:any = agc[commandClass];
              let isCommand = anyCommandClass.prototype instanceof commandBase;
              let isEvent = anyCommandClass.prototype instanceof eventBase;
              let isViewBuilder = anyCommandClass.prototype instanceof viewBuilderBase;

              if (!isCommand && !isEvent && !isViewBuilder)
                continue;

              if (isCommand) {
                let c = new anyCommandClass();
                if (c.handle === undefined)
                  continue;

                let commandName = c.command();
                let command = null;

                let commandNeedsExistingAggregate = c.aggregateIdField != undefined;

                const commandMetadata = {name: commandName, existing: commandNeedsExistingAggregate};

                command = domainHelper.defineCommand(
                  commandMetadata,
                  (data, aggregate) => new anyCommandClass(data).handle(new aggregateClass(aggregate), errors));

                aggregate.addCommand(command);

                commands[commandName] = anyCommandClass;
              } else if (isEvent) {
                new anyCommandClass();

                let eventName = _.camelCase(commandClass);
                anyCommandClass.prototype.eventName = eventName;

                let eventDefinition = null;
                eventDefinition = domainHelper.defineEvent(
                    {name: eventName},
                    (data, agg) => new anyCommandClass(data).applyEvent(new aggregateClass(agg)));

                aggregate.addEvent(eventDefinition);
              } else if (isViewBuilder && !viewBuilders[cf+commandClass]) {

                let vb = new anyCommandClass();

                const collectionName = config.get("tablePrefix") + vb.collection();
                const viewBuilderMetadata = vb.viewBuilderMetadata();
                const coll = _.find(denormalizer.tree.getCollections(), c => c.name == collectionName);

                for (const key of Object.getOwnPropertyNames(anyCommandClass.prototype)) {
                  let method = key;

                  if (/when.+/.test(method) === false)
                    continue;

                  const reflect:any = Reflect;
                  let metadata = reflect.getMetadata("design:paramtypes", vb, method);

                  if (metadata) {
                    const event = new metadata[0]();
                    const eventName = _.camelCase(event.constructor.name);

                    const finalMetadata = objectAssign(
                      {
                        id: 'payload.id', // if not defined or not found it will generate a new viewmodel with new id
                        payload: 'payload',
                        name: eventName,
                      },
                      viewBuilderMetadata);

                    const newBuilder = denormalizerHelper.defineViewBuilder(finalMetadata, function (data, vm) {
                      let partitionKey = getPartitionKey();
                      vm.set('PartitionKey', partitionKey);
                      vm.set('RowKey', vm.id.toString());

                      const viewModel = metadata[1];
                      vb[method](data, new viewModel(vm));
                    });

                    viewBuilders[cf+commandClass] = newBuilder;
                    coll.addViewBuilder(newBuilder);
                  }
                }
              }
            }
          });

          cqrsDomain.onEvent(createDispatcher(denormalizer, synchronousDenormalizer));

        });

        if (cb) {
          cb();
        }

      });
    });
  },
  handle: createHandler(getCurrentUser, handleCommand, synchronousDenormalizer)
};
