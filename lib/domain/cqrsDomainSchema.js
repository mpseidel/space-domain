"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function defineCqrsDomainSchema(cqrsDomain) {
    cqrsDomain.defineCommand({
        id: 'id',
        name: 'command',
        aggregateId: 'aggregate.id',
        payload: 'payload',
        revision: 'head.revision'
    });
    cqrsDomain.defineEvent({
        correlationId: 'commandId',
        id: 'id',
        name: 'event',
        aggregateId: 'aggregate.id',
        payload: 'payload',
        revision: 'head.revision'
    });
}
exports.defineCqrsDomainSchema = defineCqrsDomainSchema;
//# sourceMappingURL=cqrsDomainSchema.js.map