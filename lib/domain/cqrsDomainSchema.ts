export function defineCqrsDomainSchema(cqrsDomain) {
  cqrsDomain.defineCommand({
    id: 'id',
    name: 'command',
    aggregateId: 'aggregate.id',
    payload: 'payload',
    revision: 'head.revision'
  });
  cqrsDomain.defineEvent({
    correlationId: 'commandId',
    id: 'id',
    name: 'event',
    aggregateId: 'aggregate.id',
    payload: 'payload',
    revision: 'head.revision'
  });
}